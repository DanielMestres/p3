package edu.uprm.cse.datastructures.cardealer;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.model.CarTable;
import edu.uprm.cse.datastructures.cardealer.util.BTree;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("/cars")
public class CarManager {
	/*
	 * Instances
	 */
	private final BTree<Long, Car> tree = CarTable.getInstance();
	private final SortedList<Car> orderedList = CarList.getInstance();

	/*
	 * Returns a sorted array containing the cars in the tree
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		CarList.resetCars();
		Car[] result = new Car[tree.size()];
		List<Car> unSortedCars = tree.getValues();
		
		for (Car c : unSortedCars) {
			orderedList.add(c);
		}
		
		int count = 0;
		while(count != orderedList.size()) {
			result[count] = orderedList.get(count);
			count++;
		}
		
		return result;
	}
	
	/*
	 * Returns car with given id, returns an error if car entry is null
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		if (!(tree.get(id) == null))
			return tree.get(id);
		else
			throw new WebApplicationException(Response.Status.NOT_FOUND);
	}

	/*
	 * Adds car to tree, returns an error if car is null
	 */
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {
		if (!car.equals(null)) {
			tree.put(car.getCarId(), car);
			return Response.status(Response.Status.CREATED).build();
		} else
			return Response.status(Response.Status.BAD_REQUEST).build();
	}
	
	/*
	 * Deletes car with given id if its present, returns an error if car entry is null
	 */
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id) {
		if (!(tree.get(id) == null)) {
			tree.remove(tree.get(id).getCarId());
			return Response.status(Response.Status.OK).build();
		} else
			return Response.status(Response.Status.NOT_FOUND).build();
	}

	/*
	 * Replaces car that matches given id with the given car, returns an error if car is null
	 */
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(@PathParam("id") long id, Car car) {
		if (!car.equals(null)) {
			tree.put(car.getCarId(), car);
			return Response.status(Response.Status.OK).build();
		} else
			return Response.status(Response.Status.NOT_FOUND).build();
	}
}