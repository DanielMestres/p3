package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

public class CarList {
	/*
	 * Stores the list instance
	 */
	private static SortedList<Car> list = new CircularSortedDoublyLinkedList<Car>(new CarComparator());

	/*
	 * Returns the list
	 */
	public static SortedList<Car> getInstance() {
		return list;
	}

	/*
	 * Instances a new list
	 */
	public static void resetCars() {
		list = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	}
}