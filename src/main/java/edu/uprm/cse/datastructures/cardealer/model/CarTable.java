package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.BTree;
import edu.uprm.cse.datastructures.cardealer.util.TwoThreeTree;

public class CarTable {
	/*
	 * Stores the tree instance
	 */
	private static BTree<Long, Car> tree = new TwoThreeTree<Long, Car>(new LongComparator());

	/*
	 * Returns the tree instance
	 */
	public static BTree<Long, Car> getInstance() {
		return tree;
	}

	/*
	 * Instances a new tree
	 */
	public static void resetCars() {
		tree =  new TwoThreeTree<Long, Car>(new LongComparator());
	}
}