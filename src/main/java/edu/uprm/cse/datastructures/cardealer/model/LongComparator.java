package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class LongComparator implements Comparator<Long> {
	/* 
	 * Compares two Long numbers
	 * Returns a value < 0 or > 0 if they are different
	 * Returns 0 if they are equal.
	 */
	@Override
	public int compare(Long n1, Long n2) {
		return n1.compareTo(n2);
	}
}
