package edu.uprm.cse.datastructures.cardealer.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TwoThreeTree<K, V> extends BTree<K, V> {
	/*
	 * Constructor
	 */
	public TwoThreeTree(Comparator<K> keyComparator) {
		super(keyComparator);
	}
	
	/*
	 * Utility Methods, moved here for convenience
	 */
	private V getHelper(TreeNode node, K key) {
		if(this.isEmpty())
			return null;
		
		if(node != null) {
			// Edge cases
			if(!node.entries.first().deleted && node.entries.first().key.equals(key))
				return node.entries.first().value;
			
			if(!node.entries.last().deleted && node.entries.last().key.equals(key))
				return node.entries.last().value;
			
			// Recursive step
			if(this.keyComparator.compare(key, node.entries.first().key) <= 0)
				return this.getHelper(node.left, key);

			if(this.keyComparator.compare(key, node.entries.last().key) > 0)
				return this.getHelper(node.right, key);
			else
				return this.getHelper(node.center, key);
			
		} else
			return null;
	}
	
	private V putHelper(TreeNode node, K key, V val) {
		if(this.isEmpty()) {
			this.root = new TreeNode(new MapEntry(key, val, this.keyComparator), null, this.keyComparator);
			this.currentSize++;
			return val;
		}
		
		// Removes node with given key if its already present in tree
		if(this.contains(key))
			this.remove(key);
		
		if(!this.isLeaf(node)) {
			// Recursive step
			if(this.keyComparator.compare(key, node.entries.first().key) <= 0)
				return this.putHelper(node.left, key, val);
			
			if(node.entries.first() != node.entries.last()) {
				if(!(this.keyComparator.compare(key, node.entries.last().key) > 0))
					return this.putHelper(node.center, key, val);
			} else
				return this.putHelper(node.right, key, val);
		} else {
			node.entries.add(new MapEntry(key, val, this.keyComparator));
			this.currentSize++;
			
			// Doesn't do anything if node doesn't have 3 entries
			this.split(node);
		}
		return val;
	}
	
	private V removeHelper(TreeNode node, K key) {
		if(node != null) {
			// Edge cases
			if(!node.entries.first().deleted && node.entries.first().key.equals(key)) {
				V result = node.entries.first().value;
				node.entries.first().deleted = true;
				this.currentSize--;
				return result;
			} else {
				if(!node.entries.last().deleted && node.entries.last().key.equals(key)) {
					V result = node.entries.last().value;
					node.entries.last().deleted = true;
					this.currentSize--;
					return result;
				}
			}
			
			// Recursive step
			if(this.keyComparator.compare(key, node.entries.first().key) <= 0)
				return this.removeHelper(node.left, key);
			
			if(this.keyComparator.compare(key, node.entries.last().key) > 0)
				return this.removeHelper(node.right, key);
			else
				return this.removeHelper(node.center, key);
		} else
			return null;
	}
	
	private void getKeysHelper(TreeNode node, List<K> list) {
		if(node != null) {
			for(MapEntry entry : node.entries) {
				if(!entry.deleted)
					list.add(entry.key);
			}
			
			// Adds keys from nodes in center -> left -> right order
			this.getKeysHelper(node.center, list);
			this.getKeysHelper(node.left, list);	
			this.getKeysHelper(node.right, list);
		} else
			return;
	}
	
	private void getValuesHelper(TreeNode node, List<V> list) {
		if(node != null) {	
			for(MapEntry entry : node.entries) {
				if(!entry.deleted)
					list.add(entry.value);
			}
			
			// Adds values from nodes in center -> left -> right order
			this.getValuesHelper(node.center, list);
			this.getValuesHelper(node.left, list);
			this.getValuesHelper(node.right, list);
		} else
			return;
	}
	
	/*
	 * Map Class Methods
	 */
	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.currentSize <= 0;
	}

	@Override
	public V get(K key) {
		if(key != null)
			return this.getHelper(this.root, key);
		else
			throw new IllegalArgumentException("Null Argument");
	}

	@Override
	public V put(K key, V val) {
		if(key != null || val != null)
			return this.putHelper(this.root, key, val);
		else
			throw new IllegalArgumentException("Null Arguments");
	}
	
	@Override
	public V remove(K key) {
		if(key != null)
			return this.removeHelper(this.root, key);
		else
			throw new IllegalArgumentException("Null Argument");
	}
	
	@Override
	public boolean contains(K key) {
		return this.get(key) != null;
	}
	
	@Override
	public List<K> getKeys() {
		ArrayList<K> list = new ArrayList<>();
		this.getKeysHelper(this.root, list);
		return list;
	}

	@Override
	public List<V> getValues() {
		ArrayList<V> list = new ArrayList<>();
		this.getValuesHelper(this.root, list);
		return list;
	}
	
	/*
	 * BTree Class Methods
	 */
	@Override
	public void split(TreeNode node) {
		// Terminates if 3 entries are not present in node
		if(node.entries.size() != 3)
			return;
		else {
			// Edge cases
			if(node == this.root) {
				TreeNode temp = new TreeNode(this.root.entries.get(1), null, this.keyComparator);
				temp.left = new TreeNode(this.root.entries.get(0), null, this.keyComparator);
				temp.right = new TreeNode(this.root.entries.get(2), null, this.keyComparator);
	
				this.root = temp;
			} else {
				node.parent.entries.add(node.entries.get(1));
				node.entries.remove(node.entries.get(1));
				
				if(node.parent.entries.first().compareTo(node.entries.first()) < 0) {
					if(node.parent.center != null) {
						node.parent.center.entries.add(node.entries.first());
					} else
						node.parent.center = new TreeNode(node.entries.first(), null, this.keyComparator);
					
					node.entries.remove(0);
				} else {
					if(node.parent.entries.first().compareTo(node.entries.first()) >= 0) {
						if(node.parent.center != null) {
							node.parent.center.entries.add(node.entries.last());
						} else
							node.parent.center = new TreeNode(node.entries.last(), null, this.keyComparator);
						
						node.entries.remove(node.entries.size() - 1);
					}
				}
				
				// Recursive step
				this.split(node.parent);
			}
		}
	}

	@Override
	public boolean isLeaf(BTree<K, V>.TreeNode node) {
		if(node.left == null && node.center == null && node.right == null)
			return true;
		else
			return false;
	}
}